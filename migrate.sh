mysqldump --defaults-extra-file=./42checkin.src.cnf \
          --skip-triggers \
	  --insert-ignore \
	  --skip-lock-tables \
	  --single-transaction \
	  --databases enter > 42checkin.src.sql

mysql --defaults-extra-file=./42checkin.dest.cnf < 42checkin.src.sql

